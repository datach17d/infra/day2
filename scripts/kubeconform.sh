#!/bin/bash -xe

GIT_DIR=$(git rev-parse --show-toplevel)
ERROR=0
${GIT_DIR}/scripts/render_kustomize.sh || ERROR=1
OUTPUT_FILE=${OUTPUT_FILE:-/dev/stdout}

KUBECONFORM_ARGS=(-summary -verbose)

if [ "${OUTPUT_FILE}" == "results.xml" ]; then
    KUBECONFORM_CACHE_DIR=${GIT_DIR}/.cache
    if [ ! -d ${KUBECONFORM_CACHE_DIR}/.cache ]; then
        mkdir -p ${KUBECONFORM_CACHE_DIR}
    fi
    KUBECONFORM_ARGS=(-output junit -cache ${GIT_DIR}/.cache)
fi

pushd $GIT_DIR/output 
kubeconform \
    -schema-location default \
    -schema-location "https://raw.githubusercontent.com/JJGadgets/flux2-schemas/main/{{.ResourceKind}}{{.KindSuffix}}.json" \
    -schema-location "https://gitlab.com/datach17d/infra/schema/-/raw/master/schema/{{.ResourceKind}}_{{.ResourceAPIVersion}}.json" \
    ${KUBECONFORM_ARGS[@]} * > ${OUTPUT_FILE} || ERROR=1
popd

exit $ERROR
