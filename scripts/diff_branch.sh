#!/bin/bash -xe

GIT_DIR=$(git rev-parse --show-toplevel)
GIT_URL=$(git remote get-url origin)
CI_MERGE_REQUEST_TARGET_BRANCH_NAME=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME:-master}
GIT_OUTPUT_TARGET=${GIT_DIR}/output_target


if [ ! -d ${GIT_OUTPUT_TARGET} ]; then
    git clone -b ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} ${GIT_URL} ${GIT_OUTPUT_TARGET}
elif [ ! -z "${UPDATE}" ]; then
    git -C ${GIT_OUTPUT_TARGET} checkout ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
    git -C ${GIT_OUTPUT_TARGET} pull
fi

${GIT_DIR}/scripts/render_kustomize.sh
(cd ${GIT_OUTPUT_TARGET} && ${GIT_DIR}/scripts/render_kustomize.sh)

(cd ${GIT_DIR} && diff -ru output_target/output output || true)
