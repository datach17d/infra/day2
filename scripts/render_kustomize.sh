#!/bin/bash -xe

JUNIT_OUTPUT=${JUNIT_OUTPUT:-0}
GIT_DIR=$(git rev-parse --show-toplevel)
OVERLAY_PATHS=${OVERLAY_PATHS:-clusters/whome/
clusters/virt/
apps/overlays/whome
apps/overlays/virt}

OUTPUT_DIR=${GIT_DIR}/output
BUILD_ERROR=0

while read KUSTOMIZE_DIR; do
  RESULTS_DIR=${OUTPUT_DIR}/${KUSTOMIZE_DIR#${GIT_DIR}/}
  mkdir -p ${RESULTS_DIR}
  kustomize build ${GIT_DIR}/${KUSTOMIZE_DIR} > ${RESULTS_DIR}/kustomization.yaml || BUILD_ERROR=1
done <<<"$OVERLAY_PATHS"

exit ${BUILD_ERROR}
